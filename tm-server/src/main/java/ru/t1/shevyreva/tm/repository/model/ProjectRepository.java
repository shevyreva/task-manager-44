package ru.t1.shevyreva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.model.IProjectRepository;
import ru.t1.shevyreva.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedModelRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    public void clearForUser(@NotNull final String userId) {
        entityManager.remove(findAllForUser(userId));
    }

    @Override
    public void clear() {
        entityManager.remove(findAll());
    }

    @Override
    public List<Project> findAllForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.userId  = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class).getResultList();
    }

    @Override
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM Project e ORDER BY e." + sortType, Project.class).getResultList();
    }

    @Override
    public List<Project> findAllForUser(@NotNull final String userId, @Nullable final Comparator<Project> comparator) {
        @NotNull final String sortType = getSortType(comparator);
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.userId  = :userId ORDER BY e." + sortType, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Project findOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.id  = :id AND e.userId  = :userId", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public Project findOneByIndexForUser(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM Project e WHERE e.userId  = :userId", Project.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Project findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }


    @Override
    public int getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public int getSizeForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e WHERE e.userId = :userId", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneByIdForUser(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneByIdForUser(userId, id));
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public boolean existsByIdForUser(@NotNull final String userId, @NotNull final String id) {
        return findOneByIdForUser(userId, id) != null;
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
