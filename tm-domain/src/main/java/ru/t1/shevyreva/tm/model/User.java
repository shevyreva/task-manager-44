package ru.t1.shevyreva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tm_user")
@NoArgsConstructor
public final class User extends AbstractModel {

    @NotNull
    @Column(name = "login", nullable = false)
    private String login;

    @NotNull
    @Column(name = "password", nullable = false)
    private String passwordHash;

    @Nullable
    @Column(name = "email", nullable = true)
    private String email;

    @Nullable
    @Column(name = "first_name", nullable = true)
    private String firstName;

    @Nullable
    @Column(name = "last_name", nullable = true)
    private String lastName;

    @Nullable
    @Column(name = "middle_name", nullable = true)
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "locked", nullable = false)
    private Boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

}
