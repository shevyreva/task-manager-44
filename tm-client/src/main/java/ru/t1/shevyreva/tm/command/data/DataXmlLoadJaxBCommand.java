package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.data.DataLoadXmlJaxBRequest;
import ru.t1.shevyreva.tm.enumerated.Role;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load Data to xml file.";

    @NotNull
    private final String NAME = "data-load-xml-jaxb";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA XML LOAD]");
        @NotNull final DataLoadXmlJaxBRequest request = new DataLoadXmlJaxBRequest(getToken());
        getDomainEndpoint().loadXmlDataJaxB(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
