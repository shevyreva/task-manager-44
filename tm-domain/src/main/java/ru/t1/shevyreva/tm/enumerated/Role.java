package ru.t1.shevyreva.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @Nullable
    private final String displayName;

    Role(@Nullable String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        return null;
    }

    @Nullable
    public String getDisplayName() {
        return this.displayName;
    }

}
