package ru.t1.shevyreva.tm.dto.response.user;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserLogoutResponse extends AbstractUserResponse {

}
