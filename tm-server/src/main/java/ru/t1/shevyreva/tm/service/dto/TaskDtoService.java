package ru.t1.shevyreva.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.dto.ITaskDtoService;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskDtoSort;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public final class TaskDtoService implements ITaskDtoService {

    private final IConnectionService connectionService;

    public TaskDtoService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            taskRepository.addForUser(userId, task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        try {
            entityManager.getTransaction().begin();
            taskRepository.addForUser(userId, task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final TaskDtoSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            if (sort == null) return taskRepository.findAllForUser(userId);
            if (sort != null) return taskRepository.findAllForUser(userId, sort.getComparator());
            else return null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @Nullable final TaskDTO task = taskRepository.findOneByIdForUser(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            @Nullable final TaskDTO task = taskRepository.findOneByIndexForUser(userId, index);
            if (task == null) throw new TaskNotFoundException();
            return task;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.getSizeForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO removeOne(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new ModelNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeOneByIdForUser(userId, task.getId());
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public TaskDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            TaskDTO task = taskRepository.findOneByIdForUser(userId, id);
            entityManager.getTransaction().begin();
            taskRepository.removeOneByIdForUser(userId, id);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clearForUser(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return taskRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<TaskDTO> add(@NotNull final Collection<TaskDTO> models) {
        if (models == null) throw new TaskNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull TaskDTO task : models) {
                taskRepository.add(task);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        @Nullable final Collection<TaskDTO> entities;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            removeAll();
            entityManager.getTransaction().begin();
            entities = add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return entities;
    }

    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            return (taskRepository.findOneByIdForUser(userId, id) != null);
        } finally {
            entityManager.close();
        }
    }

}
